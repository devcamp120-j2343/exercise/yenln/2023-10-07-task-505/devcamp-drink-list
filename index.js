//Import thư viện express js tương đương import express from "express"
const express = require("express");

//Khởi tạo app express
const app = express();

//Khai báo cổng chạy project
const port = 8000;

//Khai báo để app đọc được bofy json
app.use(express.json());

//Callback function lá 1 function đóng vai trò là tham số của 1 function khác, nó sẽ được thực hiện khi function chủ được gọi
//Khai báo API dạng /
app.get("/api", (req, res) => {
    let today = new Date();
    res.json({
        message: `Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})

class Drink {
    constructor(id,maNuocUong,tenNuocUong,donGia, ngayTao, ngayCapNhat){
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
}
app.get("/drinks-class",(Drink)=>{
    console.log(Drink);
})

var drink1 = new Drink(1,"TRATAC", "Trà tắc", 10000,"14/5/2021",)

app.get("/drinks-object",(res) =>{
    res.json({
        message: console.log(drink1)
    })
})
